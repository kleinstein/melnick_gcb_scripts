
#PairSeq.py --version
#PairSeq.py: 0.5.10-2018.10.19
#MUSCLE v3.8.31
#usearch v7.0.1090_i86linux32
#IMGT:
#IGBLAST:

#from hg clone https://bitbucket.org/kleinstein/changeo -r "Version 0.4.5"
export PYTHONPATH=/ysm-gpfs/pi/kleinstein/melnik_gcb/changeo/changeo:$PYTHONPATH
export PATH=/ysm-gpfs/pi/kleinstein/melnik_gcb/changeo/changeo/bin:$PATH

# run runPrestoPipeline_abseq.sh for each sample
perl makePrestoFiles.pl

# filter sequences
Rscript process_filter_sequnces.R
# CONCOUNT1 cutoffs:
#	Confettie >62
#	YFP >156
#	YFP2 >174

#Mutation frequency analysis by cell type and primer
Rscript mutation_frequency_analysis.R

# Perform clonal clustering and re-assign germlines
bash runChangeo.sh

# Clonal diversity and V1-72 analysis
Rscript cloneAnalysis.R
# Trp->Leu mutations in V1-72 by mouse:
#            v172.codon33.aa
#              C  L  W  X
#  mutEZH2_29  0  0 40  0
#  mutEZH2_50  0  0 15  1
#  mutEZH2_54  0  0 10  0
#  WT_26       0  0  2  0
#  WT_27       0  1 23  0
#  WT_45       1  4 15  0

# Calculate probabilities that sequences with the same color
# combination belong to the same clone
Rscript confettiAnalysis.R

# Run tree-based analyses
Rscript treeAnalysis.R

