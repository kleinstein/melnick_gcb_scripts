#!/usr/bin/bash
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=5gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com
#SBATCH --exclude=c24n04

module load R 
module load BLAST+
module load USEARCH
module load MUSCLE

threads=4
#predir=FASTQ_Generation_2018-09-13_16_39_50Z-122717755
#postdir=YFP-1_L001-ds.302613002ba74353ace61af360a6818e
#base=test
#one=test1
#two=test2
#rm $dir/test*

predir=$1
postdir=$2
base=$3
one=$base\_R1_001
two=$base\_R2_001

rawdir=/ysm-gpfs/pi/kleinstein/melnik_gcb/raw/$predir/$postdir
dir=/ysm-gpfs/pi/kleinstein/melnik_gcb/presto/$predir/$postdir

rm $dir/*

#head -n 10000 $dir/YFP-1_S1_L001_R1_001_length-pass.fastq > $dir/test1_length-pass.fastq
#head -n 10000 $dir/YFP-1_S1_L001_R2_001_length-pass.fastq > $dir/test2_length-pass.fastq

echo $predir
echo $postdir
echo $base
echo $dir
echo $rawdir

echo `MakeDb.py --version` 

syslog=$dir/logs/sys.log

mkdir ../presto/$predir
mkdir ../presto/preprocessing
mkdir ../presto/$predir/$postdir

#mkdir $dir/keep
#rm $dir/*
mkdir $dir/logs
mkdir $dir/parsedlogs

##Filter both reads by length
FilterSeq.py length -s $rawdir/$one.fastq --log $dir/logs/one\_length.log -n \
	300 --nproc $threads --outdir $dir --outname $one > $syslog
FilterSeq.py length -s $rawdir/$two.fastq --log $dir/logs/two\_length.log -n \
	300 --nproc $threads --outdir $dir --outname $two >> $syslog

#Filter both reads by quality score 20
FilterSeq.py quality -s $dir/$one\_length-pass.fastq -q 20 \
	--log $dir/logs/one_quality.log --nproc $threads --outname $one >> $syslog

FilterSeq.py quality -s $dir/$two\_length-pass.fastq -q 20 \
	--log $dir/logs/two_quality.log --nproc $threads --outname $two >> $syslog

MaskPrimers.py score -s $dir/$one\_quality-pass.fastq \
	-p ../primers/indexes.fa --mode mask --start 2 --outname $one\_index \
	--log $dir/logs/index.log --failed --maxerror 0.0 --nproc $threads \
	>> $syslog

MaskPrimers.py score -s $dir/$one\_index_primers-pass.fastq \
	-p ../primers/Forward.fasta --mode mask --start 8 --outname $one\_forward \
	--log $dir/logs/forward.log --maxerror 0.1 --nproc $threads >> $syslog

#see if there's still anything that matches the forward primer, since we get a lot of dimers
MaskPrimers.py align -s $dir/$one\_forward_primers-pass.fastq \
	-p ../primers/Forward.fasta --mode mask --outname $one\_forward2 \
	--log $dir/logs/f2pim.log --failed --nproc $threads --maxerror 0.1 \
	--maxlen 500 \
	>> $syslog

#mask reverse primers	
MaskPrimers.py score -s $dir/$two\_quality-pass.fastq \
 	-p ../primers/R2primer.fasta --mode mask --start 0 --outname $two\_rprim \
 	--log $dir/logs/rpim.log --failed --maxerror 0.1 --nproc $threads \
 	>> $syslog

ParseHeaders.py expand -s $dir/$one\_forward2_primers-fail.fastq -f PRIMER \
 	>> $syslog

ParseHeaders.py rename -s $dir/$one\_forward2_primers-fail_reheader.fastq \
	-f PRIMER1 PRIMER2 -k INDEX PRIMER --outname $one\_reprimer >> $syslog

# #Copy index info to read 2
PairSeq.py -1 $dir/$one\_reprimer_reheader.fastq \
	-2 $dir/$two\_rprim_primers-pass.fastq \
	--coord illumina --1f INDEX >> $syslog


#AssemblePairs.py join: Concatenate the sequence pairs end-2-end to EstimateError
AssemblePairs.py align \
-1 $dir/$one\_reprimer_reheader_pair-pass.fastq \
-2 $dir/$two\_rprim_primers-pass_pair-pass.fastq \
--1f PRIMER \
--2f INDEX \
--coord illumina \
--outname $base\-index \
--nproc $threads >>$syslog

ClusterSets.py set \
-s $dir/$base\-index_assemble-pass.fastq  \
-f INDEX \
-k CLUSTER \
--ident 0.8 \
--cluster usearch \
--outname $base\-uid \
--nproc $threads \
--log $dir/logs/clustersets.log>>$syslog

ParseHeaders.py merge \
-s $dir/$base\-uid_cluster-pass.fastq \
-f INDEX CLUSTER \
-k INDEXCLUSTER \
--outname $base\-seq >>$syslog

#PairSeq.py: transfer the new barcodes over to the original read1 
PairSeq.py \
-1 $dir/$one\_reprimer_reheader_pair-pass.fastq \
-2 $dir/$base-seq_reheader.fastq \
--2f INDEXCLUSTER \
--coord presto >>$syslog

#PairSeq.py: transfer the new barcodes over to the original read1 
PairSeq.py \
-1 $dir/$two\_rprim_primers-pass_pair-pass.fastq \
-2 $dir/$base-seq_reheader.fastq \
--2f INDEXCLUSTER \
--coord presto  >>$syslog

# Build consensuses for each read
BuildConsensus.py \
-s $dir/$one\_reprimer_reheader_pair-pass_pair-pass.fastq \
--bf INDEXCLUSTER \
--pf PRIMER \
--cf INDEX \
--act set \
-q 0 \
--maxgap 0.5 \
--prcons 0.6 \
--outname $base\-R1 \
--nproc $threads \
--log $dir/logs/consensus1.log  >>$syslog

BuildConsensus.py \
-s $dir/$two\_rprim_primers-pass_pair-pass_pair-pass.fastq \
--bf INDEXCLUSTER \
--pf PRIMER \
-q 0 \
--maxgap 0.5 \
--prcons 0.6 \
--outname $base\-R2 \
--nproc $threads \
--log $dir/logs/consensus2.log  >>$syslog

ParseHeaders.py rename -s $dir/$base\-R1_consensus-pass.fastq \
	-f CONSCOUNT PRCONS -k CONSCOUNT1 PRCONS1 >>$syslog

ParseHeaders.py rename -s $dir/$base\-R2_consensus-pass.fastq \
	-f CONSCOUNT PRCONS -k CONSCOUNT2 PRCONS2 >>$syslog

#PairSeq.py: re-pair the reads after consensus building 
PairSeq.py \
-1 $dir/$base\-R1_consensus-pass_reheader.fastq \
-2 $dir/$base\-R2_consensus-pass_reheader.fastq \
--coord presto  >>$syslog


AP_ALN_SCANREV=true
AP_ALN_MAXERR=0.3
AP_ALN_MINLEN=8
AP_ALN_ALPHA=1e-5
AssemblePairs.py align \
-1 $dir/$base\-R1_consensus-pass_reheader_pair-pass.fastq \
-2 $dir/$base\-R2_consensus-pass_reheader_pair-pass.fastq \
--1f INDEX PRCONS1 CONSCOUNT1 \
--2f PRCONS2 CONSCOUNT2 \
--coord presto \
--rc tail \
--minlen ${AP_ALN_MINLEN} \
--maxerror ${AP_ALN_MAXERR} \
--alpha ${AP_ALN_ALPHA} \
--scanrev \
--failed \
--outname $base\_final \
--fasta \
--log $dir/logs/assemble.log >>$syslog


AssignGenes.py igblast -s $dir/$base\_final_assemble-pass.fasta \
	-b /ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/igblast \
	--organism mouse --loci ig --format blast --nproc $threads >> $syslog

MakeDb.py igblast -i $dir/$base\_final_assemble-pass_igblast.fmt7 \
	-s $dir/$base\_final_assemble-pass.fasta \
	-r /ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHV.fasta \
	   /ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHD.fasta \
	   /ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta \
	--outname $base --regions --scores --outdir ../changeo/db-pass/ >> $syslog

CreateGermlines.py -d ../changeo/db-pass/$base\_db-pass.tab \
	-r /ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHV.fasta \
	   /ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHD.fasta \
	   /ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta \
	-g dmask --outname $base >> $syslog