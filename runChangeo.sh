#!/env/bash

module load R

DATA_DIR=../changeo/

DefineClones.py -d $DATA_DIR/combined/Filtered_seqs.tsv --act set \
	--model ham --norm len --dist 0.1 --nproc 1 --gf MOUSE \
	--log ../changeo/combined/clones.log

CreateGermlines.py -d ${DATA_DIR}/combined/Filtered_seqs_clone-pass.tab -r \
	/ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHV.fasta \
	/ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHD.fasta \
	/ysm-gpfs/pi/kleinstein/melnik_gcb/germline_functional/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta \
	-g dmask --cloned --outdir ../changeo/germ-pass \
	--log ../changeo/combined/germlines.log
