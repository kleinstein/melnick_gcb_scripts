

use strict;
use warnings;

my @dirs = glob("../raw/*/*");

my $count=0;
foreach my $d (@dirs){
		print "$d\n";
		if($d =~ /preprocessing/){next;}
		my $pre=-1; my $post=-1;
		if($d=~/(FASTQ\S+)\/(YFP\S+)/){
			$pre=$1;
			$post=$2;
		}
		if($d=~/(FASTQ\S+)\/(Confettie\S+)/){
			$pre=$1;
			$post=$2;
		}
		if($pre eq "-1"|| $post eq "-1"){die($d)}
		my @files = glob("$d/*");
		my $base=-1;
		foreach my $f (@files){
			if($f =~ /(YFP\d*-\d+_S\d+_L001)/){
				$base=$1;
			}
			if($f =~ /\/(Confettie-\d+_S\d+_L001)_R\d_001.fastq/){
				$base=$1;
			}
		}
		print "PRE: $pre\nPOST: $post\nBASE: $base\n";
		if($base eq -1 ){next;}
		my $part= "general";
		if(($count+0) % 2 == 0){
			$part = "pi_kleinstein";
		}
		system("sbatch --partition=$part --job-name=$base runPrestoPipeline_abseq.sh $pre $post $base");
		$count++;
}
